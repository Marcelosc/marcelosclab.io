---
layout: post
title:  "MAC0472 - Agile Methods Lab - Patch submission to the Linux kernel"
date:   2018-12-02 17:01:11 -0300
ref: mac0472-patches
lang: en
---

This is a short guide of commands used to send a patch to the Linux kernel.


Warning: This is an old post with potentially outdated information. Read with
caution.
{: .warning}

Navigate to the repository directory containing the Linux kernel source code you
have made changes to:

For example:
```shell
cd ~/linux-iio/iio/
```
In this directory you will find important subdirectories like `arch`, `drivers`,
`scripts`, among others.

Use a tool to check the style of your code and make sure your changes respect
the
[kernel coding style](https://www.kernel.org/doc/html/v4.10/process/coding-style.html).

- Option number 1) Use [KWorkflow](https://github.com/rodrigosiqueira/kworkflow)'s codestyle command.
```shell
kw codestyle <file_you_changed>
```
Example:
```shell
kw codestyle drivers/staging/iio/impedance-analyzer/ad5933.c
```

- Option number 2) Use the tooling present in the kernel `scripts` dir:
```shell
perl scripts/checkpatch.pl --terse --no-tree --color=always -strict --file <file_path>
```
Example:
```shell
perl scripts/checkpatch.pl --terse --no-tree --color=always -strict --file drivers/staging/iio/impedance-analyzer/ad5933.c
```

Make sure your changes compile before submitting your code for community review.
```shell
make M=<directory_that_contains_the_files_you_changed> clean
make M=<directory_that_contains_the_files_you_changed>
```
Example:
```shell
make M=drivers/staging/iio/impedance-analyzer clean
make M=drivers/staging/iio/impedance-analyzer
```
It is also recommended to install the modified kernel in a virtual machine and start it (boot) but that is a subject for another post.

Review the things you did with git diff.
```shell
git diff <file_you_changed>
```
Example:
```shell
git diff drivers/staging/iio/impedance-analyzer/ad5933.c
```
Add the file with the changes made to the files marked for commit.
```shell
git add <file_you_changed>
```
Example:
```shell
git add drivers/staging/iio/impedance-analyzer/ad5933.c
```
Commit.
```shell
git commit -s -v
```

Write the title and commit message describing what changes you made and why.
[This post](https://chris.beams.io/posts/git-commit/) explains how to write good
commit messages.

Get the list of reviewers you should email your changes to.

- Option 1)
```shell
kw m <file_path>
```
Example:
```shell
kw m drivers/staging/iio/impedance-analyzer/ad5933.c
```
- Option 2)
```sehll
perl scripts/get_maintainer.pl <file_path>
```
Example:
```shell
perl scripts/get_maintainer.pl drivers/staging/iio/impedance-analyzer/ad5933.c
```

Get the hash of the commit before the commit you made
```shell
git log
```

Use `git format-patch` to create the email that you will send to the mailing
list of the subsystem(s) you are working on and responsible for the modified
files. Replace the emails after `--to` with the emails obtained from the
maintainers of the file(s) you changed. At the end, put the hash of the commit
prior to the commit with your changes.

```shell
git format-patch -o <path_where_to_save_the_patch_files> \\
--to="mantainer1@domain.com,mantainer2@domain2.com,mantainerN@domainN.org" \\
--cc="list1@domain.org,list2@domain.org,listN@anotherdomain.org" \\
<hash_of_the_last_commit_before_yours>
```

Change diretory to where you saved the patch files.
You can open your patch with a text editor and double-check your changes and email addresses.

Example:
```shell
vim 0001-staging-iio-ad5933-replaced-kfifo-by-triggered_buffer.patch
```

If everything is ok, you can then send your patch to the maintainers using the
an email client like [mutt](http://www.mutt.org/) or
[neomutt](https://neomutt.org/).
```shell
neomutt -H <patch_file>
```

Example:
```shell
neomutt -H 0001-staging-iio-ad5933-replaced-kfifo-by-triggered_buffer.patch
```

Okay, the patch with your changes has been sent to the maintainers so they can
review your code and maybe incorporate it in the rest of the Linux kernel source
code.
