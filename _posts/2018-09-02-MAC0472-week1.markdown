---
layout: post
title:  "MAC0472 - Agile Methods Lab - Week 1"
date:   2018-09-02 13:30:11 -0300
ref: mac0472-week1
lang: en
---

In this first week of development the goal was to prepare the environment to
make contributions to the Linux kernel. 


Warning: This is an old post with potentially outdated information. Read with
caution.
{: .warning}

To configure the development environment on Ubuntu18 I followed these steps: 

1. Install [Qemu](https://www.qemu.org/):
```shell
sudo apt-get install qemu qemu-kvm libvirt-bin
```

2. Get the latest [ArchLinux](https://www.archlinux.org/download/) version.

3. Create a virtual HD and boot ArchLinux following the steps on M.Hanny Sabbagh
[tutorial](https://fosspost.org/tutorials/use-qemu-test-operating-systems-distributions).

4. [Install ArchLinux](https://wiki.archlinux.org/index.php/Installation_guide) 
	on a qemu virtual machine (on a qemu virtual machine (here I had a lot
	of difficulty and ended up getting a virtual machine with ArchLinux
	already installed from a friend). Even though, I'll list here some
	things I did while trying to install ArchLinux. 

	numa máquina virtual do qemu (aqui eu tive bastante dificuldade e acabei por pegar 
	uma máquina virtual com o ArchLinux já instalado de um amigo).
	Mesmo assim vou listar aqui algumas coisas que eu fiz enquanto tentava
	instalar o ArchLinux.

	4.1 I followed some of the steps from this
	[video tutorial](https://www.youtube.com/watch?v=nv0CjGdOLxY) and from
	this [post](https://www.ostechnix.com/install-arch-linux-latest-version/) 
	though have not completed any of them.

	4.2 Some usefull commands to setup the virtual machine were:

	```shell
	loadkeys br-abnt2.map.gz
	```

	To configure the keyboard to brazilian ABNT2 standard.

	```shell
	setfont iso02-12x22
	```
	To change the shell font size and style so that text gets more readable.

5. Install and setup [vim](https://www.vim.org/)

	5.1 
	```shell 
	sudo apt-get install vim 
	```
	5.2 Clone and instala configuration from  LAPPIS:
	```shell 
	git clone https://github.com/lappis-tools/lappis_vimrc.git
	```
	```shell 
	cd lappis_vimrc/
	```
	```shell 
	./install.sh -i
	```
	Edit ~/.vimrc at lines 78 e 79
	```shell 
	elseif match($XDG_CURRENT_DESKTOP, "GNOME") != -1
	  set term=xterm-256color
	```
	5.3 Find a funny vim tutorial to learn how to user this popular editor.
	[vim-adventures](https://vim-adventures.com/) (too bad the full version
	of it is paid)

6. Install and configure [neomutt](https://neomutt.org/) e-mail client 

	6.1
	```shell 
	apt-get install neomutt
	```
	6.2 Get useful configuration files from Rodrigo Siqueira
	```shell 
	wget https://raw.githubusercontent.com/rodrigosiqueira/myConfigFiles/master/roles/neomutt/files/mutt/gmail
	wget https://raw.githubusercontent.com/rodrigosiqueira/myConfigFiles/master/roles/neomutt/files/mutt/mutt-colors-solarized-light-256.muttrc
	```
	6.3 Copy the files ``gmail`` and ``mutt-colors-solarized-light-256.muttrc``
	to the mutt configuration directory
	```shell
	mv gmail ~/.mutt/
	mv mutt-colors-solarized-light-256.muttrc ~/.mutt/
	```
	6.4 Edit mutt configuration file with the information from your e-mail account
	```shell
	vim ~/.mutt/gmail 
	```
	6.5 Edit the gmail configuration to enable IMAP access and to enable
	less secure apps to access your account.
