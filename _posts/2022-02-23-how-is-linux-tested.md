---
layout: post
title:  "How are Linux device drivers being tested?"
date:   2022-03-28 14:21:11 -0300
ref: linux-driver-testing
lang: en
---

Device drivers are a substantial part of the Linux kernel accounting for about
66% of the project's lines of code. Testing these components is fundamental to
providing confidence to the operation of GNU/Linux systems under diverse
workloads. Testing is so essential that it is considered part of the software
development cycle. However, testing device drivers may be hard due to many
possible drawbacks such as not exposing a user space interface, architecture
dependence, requirement of custom configuration symbols, etc. A fact that
instigates to question: how are device drivers being tested?


<!--begin-references-->

{% include add_ref.html id="foundationsSoftwareTesting"
    author="Aditya P. Mathur"
    title="Foundations of Software Testing"
    publisher="Pearson India"
    year="2013"
    url="https://www.oreilly.com/library/view/foundations-of-software/9788131794760/" %}

{% include add_ref.html id="ieeeGlossary"
    journal="IEEE Std 610.12-1990"
    title="IEEE Standard Glossary of Software Engineering Terminology"
    year="1990"
    pages="1-84"
    url="https://ieeexplore.ieee.org/document/159342" %}

{% include add_ref.html id="kernelTestGuide"
    title="Kernel Testing Guide"
    url="https://www.kernel.org/doc/html/latest/dev-tools/testing-overview.html" %}

{% include add_ref.html id="testOverviewDiscussion"
    title="Re: [PATCH v3] Documentation: dev-tools: Add Testing Overview"
    url="https://lore.kernel.org/linux-doc/CABVgOS=2iYtqTVdxwH=mcFpcSuLP4cpJ4s6PKP4Gc-SH6jidgQ@mail.gmail.com/" %}

{% include add_ref.html id="kdoc:kselftest"
    title="Linux Kernel Selftests"
    year="2021"
    url="https://www.kernel.org/doc/html/latest/dev-tools/kselftest.html" %}

{% include add_ref.html id="lf:kselftest"
    author="Shuah Khan"
    title="Kernel Validation With Kselftest"
    year    = "2021"
    url     = "https://linuxfoundation.org/webinars/kernel-validation-with-kselftest/" %}

{% include add_ref.html id="selftest:start"
    title   = "Kernel self-test"
    year    ="2019"
    url     = "https://kselftest.wiki.kernel.org/"
%}

{% include add_ref.html id="kdoc:rcu"
 author=""
 organization="The kernel development community"
 title="A Tour Through RCU’s Requirements"
 year="2021"
 url="https://www.kernel.org/doc/html/latest/RCU/Design/Requirements/Requirements.html"
%}

{% include add_ref.html id="kdoc:xpad"
 author=""
 organization="The kernel development community"
 title="xpad - Linux USB driver for Xbox compatible controllers"
 year="2021"
 url="https://www.kernel.org/doc/html/latest/input/devices/xpad.html"
%}

{% include add_ref.html id="lj:lk-testing"
 author="Shuah Khan"
 title="Linux Kernel Testing and Debugging"
 year="2014"
 url="https://www.linuxjournal.com/content/linux-kernel-testing-and-debugging"
%}

{% include add_ref.html id="lwn:stable-stab"
 author="Jake Edge"
 title="Maintaining stable stability"
 year="2020"
 url="https://lwn.net/Articles/825536/"
%}

{% include add_ref.html id="lwn:5.12statistics"
 author="Jonathan Corbet"
 title="Some 5.12 development statistics"
 year="2021"
 url="https://lwn.net/Articles/853039/"
%}

{% include add_ref.html id="lf:syzkaller"
 author="Andrey Konovalov"
 title="Fuzzing Linux Kernel"
 year="2021"
 url="https://linuxfoundation.org/webinars/fuzzing-linux-kernel/"
%}

{% include add_ref.html id="ltp:howto"
 author="Manoj Iyer"
 title="LTP HowTo"
 year="2012"
 url="http://ltp.sourceforge.net/documentation/how-to/ltp.php"
%}

{% include add_ref.html id="smatch:intro"
 author=""
 organization=""
 title="Smatch The Source Matcher"
 year="2021"
 url="http://smatch.sourceforge.net/"
%}

{% include add_ref.html id="oracle:ktest"
 author="Daniel Jordan"
 title="So, you are a Linux kernel programmer and you want to do some automated testing..."
 organization="Oracle"
 year="2021"
 url="https://blogs.oracle.com/linux/ktest"
%}

{% include add_ref.html id="elinux:ktest"
 author=""
 title="Ktest"
 organization="Embedded Linux Wiki"
 year="2017"
 url="https://elinux.org/Ktest"
%}

{% include add_ref.html id="syzkaller:syzbot"
 author="Dmitry Vyukov and Andrey Konovalov and Marco Elver"
 title="syzbot"
 year="2021"
 url="https://github.com/google/syzkaller/blob/master/docs/syzbot.md"
%}

{% include add_ref.html id="rethinkit:kernelci"
 author=""
 organization="reTHINKit Media"
 title="Distributed Linux Testing Platform KernelCI Secures Funding and Long-Term Sustainability as New Linux Foundation Project"
 subtitle=""
 year="2019"
 url="https://www.prnewswire.com/news-releases/distributed-linux-testing-platform-kernelci-secures-funding-and-long-term-sustainability-as-new-linux-foundation-project-300945978.html"
%}

{% include add_ref.html id="lf:shuan"
 author=""
 organization="The Linux Foundation"
 title="Linux Kernel Developer: Shuah Khan"
 year="2017"
 url="https://linuxfoundation.org/blog/linux-kernel-developer-shuah-khan/"
%}

{% include add_ref.html id="lwn:tuxmake"
 author="Dan Rue"
 organization=""
 title="Portable and reproducible kernel builds with TuxMake"
 year="2021"
 url="https://lwn.net/Articles/841624/"
%}

{% include add_ref.html id="kdoc:joystick"
 author=""
 organization="The kernel development community"
 title="Linux Joystick support - Introduction"
 subtitle=""
 year="2021"
 url="https://www.kernel.org/doc/html/latest/input/joydev/joystick.html"
%}

{% include add_ref.html id="lc:kernelci"
 author="Mark Filion"
 organization=""
 title="How Continuous Integration Can Help You Keep Pace With the Linux Kernel"
 subtitle=""
 year="2016"
 url="https://www.linux.com/audience/enterprise/how-continuous-integration-can-help-you-keep-pace-linux-kernel/"
%}

{% include add_ref.html id="lf2020:hist"
 author=""
 organization="The Linux Foundation"
 title="2020 Linux Kernel History Report"
 year="2020"
 url="https://linuxfoundation.org/wp-content/uploads/2020_kernel_history_report_082720.pdf"
%}

{% include add_ref.html id="lf2017:report"
 author="Jonathan Corbet, Greg Kroah-Hartman"
 organization="LWN.net, The Linux Foundation"
 title="2017 Linux Kernel Development Report"
 year="2017"
 url="https://www.linuxfoundation.org/wp-content/uploads/linux-kernel-report-2017.pdf"
%}

{% include add_ref.html id="tuxmake:readme"
 author="Dan Rue, Antonio Terceiro"
 organization="Linaro"
 title="Linaro/tuxmake - README.md"
 year="2021"
 url="https://gitlab.com/Linaro/tuxmake"
%}

{% include add_ref.html id="kernelci:home"
 author=""
 organization="KernelCI"
 title="Welcome to KernelCI"
 year="2021"
 url="https://kernelci.org/"
%}

{% include add_ref.html id="linaro:systest"
 author=""
 title="Rapid Operating System Build and Test"
 organization="Linaro"
 year="2021"
 url="https://www.linaro.org/os-build-and-test/"
%}

{% include add_ref.html id="bottest:coccicheck"
 author="Luis R. Rodriguez, Nicolas Palix"
 title="coccicheck [Wiki]"
 organization=""
 year="2016"
 url="https://bottest.wiki.kernel.org/coccicheck"
%}

{% include add_ref.html id="bottest:start"
 author="Luis R. Rodriguez, Tyler Baker, Valentin Rothberg"
 title="linux-kernel-bot-tests - start [Wiki]"
 organization=""
 year="2016"
 url="https://bottest.wiki.kernel.org/"
%}

{% include add_ref.html id="lkp:description"
 author=""
 organization=""
 title="Linux Kernel Performance"
 year="2021"
 url="https://01.org/lkp"
%}

{% include add_ref.html id="linuxfuzzer:readme"
 author="Dave Jones"
 organization=""
 title="Linux system call fuzzer - README"
 year="2017"
 url="https://github.com/kernelslacker/trinity"
%}

{% include add_ref.html id="lwn:trinity"
 author="Michael Kerrisk"
 organization=""
 title="LCA: The Trinity fuzz tester"
 year="2013"
 url="https://lwn.net/Articles/536173/"
%}

{% include add_ref.html id="lwn:5.4statistics"
 author="Jonathan Corbet"
 title="Statistics from the 5.4 development cycle"
 year="2019"
 url="https://lwn.net/Articles/804119/"
%}

{% include add_ref.html id="linaro:lkft"
 author=""
 organization="Linaro"
 title="Linaro’s Linux Kernel Functional Test framework"
 year="2021"
 url="https://lkft.linaro.org/"
%}

{% include add_ref.html id="linaro:lkft-tests"
 author=""
 organization="Linaro"
 title="Tests in LKFT"
 year="2021"
 url="https://lkft.linaro.org/tests/"
%}

{% include add_ref.html id="coccinelle:home"
 author  = ""
 title   = "Coccinelle: A Program Matching and Transformation Tool for Systems Code"
 organization = ""
 year    = "2022"
 url     = "https://coccinelle.gitlabpages.inria.fr/website/"
%}

{% include add_ref.html id="jstest:manual"
 author  = "Stephen Kitt"
 title   = "jstest - joystick test program"
 organization = ""
 year    = "2009"
 month   = "April"
 url     = "https://sourceforge.net/p/linuxconsole/code/ci/master/tree/docs/jstest.1"
%}

{% include add_ref.html id="Renzelmann2012"
  author="Matthew J. Renzelmann and Asim Kadav and Michael M. Swift"
  booktitle="10th USENIX Symposium on Operating Systems Design and Implementation (OSDI ’12)"
  title="SymDrive: Testing Drivers without Devices"
  year="2012"
  pages="279-292"
%}

{% include add_ref.html id="symdrive:setup"
 author="Matthew J. Renzelmann and Asim Kadav and Michael M. Swift"
 title="SymDrive Download and Setup"
 year="2012"
 url="https://research.cs.wisc.edu/sonar/projects/symdrive/downloads.shtml"
%}

{% include add_ref.html id="s2e:envdoc"
 author="Cyberhaven"
 title="Creating analysis projects with s2e-env - S2E 2.0 documentation"
 year="2020"
 url="http://s2e.systems/docs/s2e-env.html"
%}

{% include add_ref.html id="s2e:envgit"
 author="Adrian Herrera"
 title="s2e-env/README.md at master - S2E/s2e-env"
 year="2020"
 url="https://github.com/S2E/s2e-env/blob/master/README.md"
%}

{% include add_ref.html id="s2e:building"
 author="Cyberhaven"
 title="Building the S2E platform manually - S2E 2.0 documentation"
 year="2020"
 url="http://s2e.systems/docs/BuildingS2E.html"
%}

{% include add_ref.html id="Buchacker2001"
  author="Buchacker, K. and Sieh, V."
  booktitle="Proceedings Sixth IEEE International Symposium on High Assurance Systems Engineering. Special Topic: Impact of Networking"
  title="Framework for testing the fault-tolerance of systems including OS and network aspects"
  year="2001"
  pages="95-105"
%}

{% include add_ref.html id="Cong2015"
author="Cong, Kai and Lei, Li and Yang, Zhenkun and Xie, Fei"
title="Automatic Fault Injection for Driver Robustness Testing"
year="2015"
url="https://doi.org/10.1145/2771783.2771811"
booktitle="Proceedings of the 2015 International Symposium on Software Testing and Analysis"
pages="361–372"
numpages="12"
%}

{% include add_ref.html id="Bai2016"
  author="Jia-Ju Bai and Yu-Ping Wang and Jie Yin and Shi-Min Hu"
  booktitle="Proceedings of the 2016 USENIX Annual Technical Conference, USENIX ATC 2016"
  title="Testing Error Handling Code in Device Drivers Using Characteristic Fault Injection"
  year="2016"
  pages="635-647"
%}

{% include add_ref.html id="Chen2020"
  author="Chen, Bo and Yang, Zhenkun and Lei, Li and Cong, Kai and Xie, Fei"
  booktitle="2020 IEEE 27th International Conference on Software Analysis, Evolution and Reengineering (SANER)"
  title="Automated Bug Detection and Replay for COTS Linux Kernel Modules with Concolic Execution"
  year="2020"
  pages="172-183"
%}

{% include add_ref.html id="Rothberg2016"
author="Rothberg, Valentin and Dietrich, Christian and Ziegler, Andreas and Lohmann, Daniel"
title="Towards Scalable Configuration Testing in Variable Software"
year="2016"
url="https://doi.org/10.1145/3093335.2993252"
pages="156–167"
numpages="12"
%}

{% include add_ref.html id="kdoc:sparse"
 author=""
 organization="The kernel development community"
 title="Sparse"
 year="2022"
 url="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/dev-tools/sparse.rst?h=v5.17-rc7"
%}

{% include add_ref.html id="lwn:sparse"
 author="Neil Brown"
 title="Sparse: a look under the hood"
 year="2016"
 url="https://lwn.net/Articles/689907/"
%}

{% include add_ref.html id="sdoc:welcome"
 author=""
 title="Welcome to sparse’s documentation"
 year="2022"
 url="https://sparse.docs.kernel.org/en/latest/"
%}

<!--end-references-->

To answer this question is the main goal of my master's research project at the
Institute of Mathematics and Statistics of the University of São Paulo
(IME-USP). To provide a comprehensive answer to the question, I resorted to a
systematic mapping study, and to a grey literature review. With the former, I
gathered information from peer-reviewed articles published by academic media
while the latter provided data from online publications sponsored by reputable
organizations and magazines.

There is a variety of ways to test software, though. One may provide a program
with all possible inputs (random testing / fuzzing), run only a few parts of the
program (unit testing, integration testing), run a program under extreme
conditions (stress testing), analyze the code semantics (semantic check, static
analysis), or apply any other of many software testing techniques. This is
because software testing "*is the process of determining if a program behaves as
expected.*" {% include cite.html id="foundationsSoftwareTesting" %} "*An
activity in which a system or component is executed under specified conditions,
the results are observed or recorded, and an evaluation is made of some aspect
of the system or component.*" {% include cite.html id="ieeeGlossary" %}

It is arguable that sometimes we don't even need to run a program to estimate if
it's going to work as desired. If we relax a little bit our concept of software
testing, we may consider code review as a sort of testing as well. "*Code
walkthrough, also known as peer code review, is used to review code and may be
considered as a static testing technique.*"
{% include cite.html id="foundationsSoftwareTesting" %} However, assessing every
such testing practice would be a cumbersome task, one that would not fit in a
master's program. So, to limit the scope of this investigative work, we (my
advisors and I) decided to look only at a subset of the tools used for kernel
testing. Precisely, the tools that enabled device driver testing.

This article presents a discussion about some of the tools employed to test
Linux device drivers. The tools addressed here are the ones most cited by
publications assessed in our mapping study and grey literature review.
Nevertheless, these tools do not portray all solutions available for kernel
testing, nor do they encompass all the possible approaches for driver testing.
The details of our research methods are available
[here](/assets/pdf/thesis-quali.pdf).

## Linux Kernel Testing Tools

Our answer to the question of how Linux drivers are being tested is going to
stem from the assessment of the tools that are said to enable driver testing. In
fact, we derived a couple of subquestions to guide ourselves throughout this
investigation. What testing tools are being used by the Linux community to test
the kernel? What are the main features of these tools? As we need to get to know
the testing apparatus to evaluate their role in assessing the functioning of
device drivers, we decided to make a catalog to synthesize the information about
these testing tools. Thus, one of our goals is to catalog the available Linux
kernel testing tools, their characteristics, how they work, and in what contexts
they are used.

Besides, I'd like to give something back to the kernel community. For that, I
intend to use the testing tool catalog to provide advice to fellow kernel
developers interested in enhancing their workflow with the use of testing tools.
Also, there is a Kernel Testing Guide {% include cite.html id="kernelTestGuide" %}
documentation page that could benefit from our work. Moreover, a mailing list
thread {% include cite.html id="testOverviewDiscussion" %} indicates that there
is a desire for a more complete testing guide.

>> Thank for you writing this much needed document.
>
>Thanks, Shuah: I hope I haven't misrepresented kselftest too much. :-)
>
>> Looks great. How about adding a section for Static analysis tools?
>> A mention coccicheck scripts and mention of smatch?
>
>Good idea. I agree it'd be great to have such a section, though I
>doubt I'm the most qualified person to write it. If no one else picks
>it up, though, I can try to put a basic follow-up patch together when
>I've got some time.

To build the proposed test tool catalog, we carried out an evaluation process to
assess the usage of each testing tool selected throughout our study. We searched
each project's repository, looked their documentation for instructions on how to
install and use each tool, installed them, and, finally, made basic use of the
testing tools. Moreover, we reached every author by email when we faced setbacks
in using those testing tools. We also visited the commit history of some
projects and their corresponding mailing lists between 2022-01-12 and
2022-01-24.

Let us finally talk about those testing tools.

## What are the options?

To some extent, several tools can facilitate device driver testing. It's nearly
impossible to analyze them all. Yet, this post covers the twenty Linux kernel
testing tools selected by our study for being either focused on driver testing
or most cited by online publications. These tools make up an heterogeneous group
of test solutions comprising diverse features and testing techniques. From unit
testing to end-to-end testing, dynamic or static analysis, many ways of puting
Linux to the test have been conceived. The following table tries to outline
test types and tools associated with them.

The checks match tests and tools according to what we found expressly reported
in the literature, with a few complementary marks added by me. Also, some
testing types interleave with each other. For instance, unit testing may be
considered a sort of regression testing, fuzzing is also an end-to-end test,
fault injection tools often instrument the source code to trigger error paths.
Thus, it's more than possible that some tools be not marked with all types of
tests they can provide. If you think that's the case, feel free to leave a
comment at the end of the page. Improvement suggestions are greatly appreciated.

<!--
| Types of Tests              | Tools |
| :-------------------------- |:---------------------------------------:|
| Unit testing                | Kselftest, KUnit  |
| Regression testing          | Kselftest  |
| Stress testing              | Kselftest, LTP  |
| Functional testing          | LKFT |
| Fuzz testing                | Trinity, Syzkaller/Syzbot, ktest |
| Reliability testing         | LTP  |
| Robustness testing          | LTP  |
| Stability testing           | LTP  |
| End-to-end testing          | Kselftest, ktest, jstest  |
| Build testing               | ktest, TuxMake |
| Static analysis             | Smatch, Coccinelle/coccicheck, Sparse |
//| Black box                   | jstest  |
| Symbolic execution          | SymDrive  |
| Fault injection             | FAUMachine, ADFI, EH-Test |
| Code Instrumentation        | ADFI, COD  |
| Concolic Execution          | COD  |
| Local analysis and grouping | Troll |
-->

<!--
TODO check what test techniques were expressely attributed to the tools and which were attributed as a result of my interpretation.
TODO add functional testing
-->

| \\ \_\_\_\_\_\_\_\_ Tool Test Type \ | Kselftest | 0-day | KernelCI | LKFT | Trinity | Syzkaller | LTP | ktest | Smatch | Coccinelle | jstest | TuxMake | Sparse | KUnit | SymDrive | FAUmachine | ADFI | EH-Test | COD | Troll |
| :-------------------------- |:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| Unit testing (2)            |<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|||||||||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|||||||
| Regression testing (1)      |<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||||||||||||||||||||
| Stress testing (2)          |<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||||||||||||||
| Fuzz testing (3)            |||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|||||||||||||
| Reliability testing (1)     |||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||||||||||||||
| Robustness testing (1)      |||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||||||||||||||
| Stability testing (1)       |||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||||||||||||||
| End-to-end testing (3)      |<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||||||||||
| Build testing (2)           ||||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|||||||||
| Static analysis (3)         |||||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||||||||
| Symbolic execution (1)      |||||||||||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||||||
| Fault injection (1)         ||||||||||||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|||
| Code Instrumentation (2)    |||||||||||||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||
| Concolic Execution (1)      |||||||||||||||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">||
| Local analysis and grouping (1) ||||||||||||||||||||<img width="25px" height="20px" src="{{ site.baseurl }}/assets/images/check.png">|

<!-- Icons from https://svgsilh.com/4caf50/image/297738.html -->

To give an overview of how easy (or not) it is to use these tools, I made a
chart of setup versus usage effort. First, I defined a set of tasks that
comprise the work of setting up a test tool. Then, I counted how many of these
tasks I carried on while setting up the considered tools. A test tool got a
point if I had to:
- A) install system packages (+1), because it was needed to install packages
  other than the ones required to build the Linux kernel.
- B) download and build a source code repository (+1), because it was needed to
  download the source code and build it locally.
- C) create a VM (+1), because the tests were potentially destructive and could
  cause problems to the running system.
- D) configure a VM (+1), because it was needed to do additional VM
  configuration such as installing packages, enabling ssh, messing up with grub,
  etc.
- E) write/edit configuration files (+1), because it was needed to create or
  modify configuration files for the tests to run.

Finally, I gave five points to the tools I could not set up after trying all the
above. The effort ratings were set to None for tools with no effort point, Low
for tools with one or two points, Moderate for tools with three to four points,
and High for tools with five points.

The classification of usage effort took into account if:
- R) the tests can run as an usual application program (+1).
- S) the tests have to run inside a VM (+1), due to risk of compromising the running system.
- T) the tests require a high amount of CPU or memory to run (+1).

In some cases, I couldn't get test results because I could not run the tests
myself or the test results were unavailable to me. Those tools got three effort
points. Lastly, the usage effort ratings were set to None, Low, Moderate, and
High for tools with zero, one, two, and three points, respectively.

<!--
To run a VM causes an additional usage effort by itself because it consumes a
certain amount of host CPU time and system memory. Though, one may configure a
VM with limited resources so that it doesn't hurt much the performance of other
activities running on the host system. If, on the contrary, a tool requires high
amounts of CPU time or system memory, then it will clearly be a higher burden to
run the tests/tasks.
-->

{% include add_image.html
   src="tool_effort.png"
   caption="Setup and usage effort of Linux kernel testing tools." %}

The next table shows the exact traits that had led me to classify the testing
tools the way they appear in the above picture. The cases where I was unable to
setup or to run a tool are indicated with a U. Also, there are two slight
adjustments I'd like to note. First, I gave an additional D to ktest because it
took me almost three days to setup everything needed to run it.  Second, I added
two extra points to FAU machine since its documentation was completely outdated
and unusable.

<!--
Setup effort:
* Kselftest: A
* 0-day: -
* KernelCI: -
* LKFT: -
* Trinity: C, B
* Syzkaller: A, B, E
* LTP: C, D, B
* ktest: A, C, D, E, extra D
* Smatch: A, B
* Coccinelle: A
* jstest: A
* TuxMake: A
* Sparse: A
* KUnit: -
* SymDrive: U
* FAU machine: A, B, E
* ADFI: U
* EH-Test: U
* COD: U
* Troll: A, B, E
-->

<!--
Usage effort:
* Kselftest: R
* 0-day: *
* KernelCI: *
* LKFT: *
* Trinity: R, S
* Syzkaller: R, S
* LTP: R, S
* ktest: R, S
* Smatch: R
* Coccinelle: R
* jstest: R
* TuxMake: R
* Sparse: R
* KUnit: R
* SymDrive: U
* FAU machine: R, S
* ADFI: U
* EH-Test: U
* COD: U
* Troll: R, T
-->

| Tool          | Setup Effort Points | Usage Effort Points  |
| ------------- |:-------------------:| :-------------------:|
| Kselftest     |    A                |   R                  |
| Trinity       |    B, C             |   R, S               |
| Syzkaller     |    A, B, E          |   R, S               |
| LTP           |    B, C, D          |   R, S               |
| ktest         | A, C, D, E, extra D |   R, S               |
| Smatch        |    A, B             |   R                  |
| Coccinelle    |    A                |   R                  |
| jstest        |    A                |   R                  |
| TuxMake       |    A                |   R                  |
| Sparse        |    A                |   R                  |
| KUnit         |        -            |   R                  |
| SymDrive      |        U            |   U                  |
| FAU machine   |    A, B, E, +2      |   R, S               |
| ADFI          |        U            |   U                  |
| EH-Test       |        U            |   U                  |
| COD           |        U            |   U                  |
| Troll         |    A, B, E          |   R, T               |

One may, of course, discord from these criteria in many ways. The next section
presents some evidence to support my considerations about these tools. Anyhow,
if you're unhappy with the information given here or feel like a tool was too
misrepresented, don't hesitate to write a comment at the end of the post.

<!--
TODO add section: Any advise?
-->

## Tool Characterization

This section briefly describes each evaluated test tool and tells the experience
we had when using them for testing the Linux kernel.

<!--

To: corbet@lwn.net, mchehab+huawei@kernel.org, dlatypov@google.com, davidgow@google.com
CC: linux-doc@vger.kernel.org, linux-sparse@vger.kernel.org, cocci@inria.fr, smatch@vger.kernel.org, linux-kernel@vger.kernel.org, skhan@linuxfoundation.org


CC: skhan@linuxfoundation.org

Jonathan Corbet <corbet@lwn.net> (maintainer:DOCUMENTATION,commit_signer:2/3=67%)
Mauro Carvalho Chehab <mchehab+huawei@kernel.org> (commit_signer:1/3=33%,authored:1/3=33%,added_lines:9/155=6%,removed_lines:9/9=100%)
Daniel Latypov <dlatypov@google.com> (commit_signer:1/3=33%)
Marcelo Schmitt <marcelo.schmitt1@gmail.com> (commit_signer:1/3=33%,authored:1/3=33%,added_lines:29/155=19%)
David Gow <davidgow@google.com> (commit_signer:1/3=33%,authored:1/3=33%,added_lines:117/155=75%)
linux-doc@vger.kernel.org (open list:DOCUMENTATION)
linux-kernel@vger.kernel.org (open list
-->

### Kselftest

Kernel selftests (kselftest) is a unit and regression test suite distributed
with the Linux kernel tree under the *tools/testing/selftests/* directory
{% include cite.html id="kdoc:kselftest" %}
{% include cite.html id="lf:kselftest" %}
{% include cite.html id="selftest:start" %}.
Kselftest contains tests for various kernel features and sub-systems such as
tests for breakpoints, cpu-hotplug, efivarfs, ipc, kcmp, memory-hotplug, mqueue,
net, powerpc, ptrace, rcutorture, timers, and vm sub-systems {% include
cite.html id="lj:lk-testing" %}. These tests are intended to be small
developer-focused tests that target individual code paths and short-running
units supposed to terminate in a timely fashion of 20 minutes {% include
cite.html id="kselftest" %}. Kselftest consists of shell scripts and user-space
programs that test kernel API and features. Test cases may span kernel and
use-space programs working in conjunction with a kernel module to test {%
include cite.html id="lf:kselftest" %}. Even though kselftest’s main purpose is
to provide kernel developers and end-users a quick method of running tests
against the Linux kernel, the test suite is run every day on several Linux
kernel integration test rings such as the 0-Day robot and Linaro Test Farm {%
include cite.html id="selftest:start" %}. It is stated that someday Kselftest
will be a comprehensive test suite for the Linux kernel
{% include cite.html id="lf:shuan" %} {% include cite.html id="lf2017:report" %}.

We had a smooth experience while using kselftest. There are recent patches and
ongoing discussions on the project [mailing list](https://lore.kernel.org/linux-kselftest/)
as well as recent commits in the subsystem tree. The documentation presents all
the instructions necessary to compile and run the tests. There are also sections
exemplifying how to run only subsets of the tests. Some kselftest tests require
additional libraries, listed with the *kselftest\_deps.sh* script.
Unfortunately, by the date we evaluated the tool, the documentation did not
mention the build dependencies script.  Nevertheless, kselftest documentation
had enough information for us to run some tests without any problem.

### 0-day test robot

The 0-day test robot is a test framework and infrastructure that runs several
tests over the Linux kernel, covering core components such as virtual memory
management, I/O subsystem, process scheduler, file system, network, device
drivers, and more {% include cite.html id="lkp:description" %}. Static analysis
tools such as sparse, smatch, and coccicheck are run by 0-day as well
{% include cite.html id="lf2020:hist" %}. These tests are provided by Intel as a
service that picks up patches from the mailing lists and tests them, often
before they are accepted for inclusion {% include cite.html id="lf2017:report" %}.
0-day also test key developers’ trees before patches move forward in the
development process. The robot is accounted for finding 223 bugs during a
development period of about 14 months from Linux release 4.8 to Linux 4.13
(which came out September 3, 2017). With that, the 0-day robot achieved the rank
of top bug reporter for that period {% include cite.html id="lf2017:report" %}.
Analyzing Linux 5.4 development cycle, though,
Corbet {% include cite.html id="lwn:5.4statistics" %} reports that there have
been worries that Intel's 0-day testing service is not proving as useful as it
once was.

### KernelCI

KernelCI is an efort to test upstream Linux kernels in a continuous integration
(CI) fashion. The project main goal is to improve the quality, stabiblity and
long-term maintenance of the Linux kernel. It is a community-led test system
that follows an open philoshophy to enable the same collaboration to happen with
testing as open source does to the code itself
{% include cite.html id="rethinkit:kernelci" %}
{% include cite.html id="kernelci:home" %}. KernelCI generates various
configurations for different kernel trees, submits boot jobs to several labs
around the world, collects, and stores test results into a database. The test
database kept by KernelCI includes tests run natively by KernelCI, but also Red
Hat's CKI, Google's syzbot and many others
{% include cite.html id="lc:kernelci" %}{% include cite.html id="kernelci:home" %}.

### LKFT

Linaro's LKFT (Linux Kernel Functional Testing) is an automated test
infrastructure that builds and tests Linux release candidates on the arm and
arm64 hardware architectures {% include cite.html id="lf2020:hist" %}. The
mission of LKFT is to improve the quality of Linux by performing functional
testing on real and emulated hardware targets. Weekly, LKFT runs tests over 350
release-architecture-target combinations on every git-branch push made to the
latest 6 Linux long-term-stable releases. In addition, Linaro claims that their
test system can consistently report results from nearly 40 of these test setup
combinations in under 48 hours {% include cite.html id="linaro:systest" %}
{% include cite.html id="linaro:lkft" %}. LKFT incorporates and runs tests from
several test suites such as LTP, kselftest, libhugetlbfs, perf, v4l2-compliance
tests, KVM-unit-tests, SI/O Benchmark Suite, and KUnit
{% include cite.html id="linaro:lkft-tests" %}.

### Trinity

Trinity is a random tester (fuzzer) that specializes in testing the system call
interfaces that the Linux kernel presents to user space
{% include cite.html id="lwn:trinity" %}. Trinity employs some techniques to
pass semi-intelligent arguments to the syscalls being called. For instance, it
accepts a directory argument from which it will open files and pass the
corresponding file descriptors to system calls under test. This can be useful
for discovering failures in filesystems. Thus, Trinity can find bugs in parts of
the kernel other than the system call interface. Some areas where people used
Trinity to find bugs include the networking stack, virtual memory code, and
drivers {% include cite.html id="linuxfuzzer:readme" %}
{% include cite.html id="lwn:trinity" %}.

Trinity is accessible through a
[repository](https://github.com/kernelslacker/trinity) on GitHub. The latest
commit to that repository is from about 1.5 months behind the repository
inspection date. From the recent commit history, we estimate that the project
change rate is roughly one commit per month and that the tool has been
maintained by three core developers. Trinity documentation is scarce and has
not been updated for four years. Although there are some usage examples, the
documentation does not contain a tool installation guide.

In our experience with Trinity, we let the fuzzer run for a few minutes. It
looks like Trinity is still working the same way Konovalov
{% include cite.html id="lf:syzkaller" %} described: "*Trinity is a kernel
fuzzer that keeps making system calls in an infinite loop.*" There is no precise
number of tests to run and no time limit for their completion. After being
interrupted, the program shows the number of executed system calls, how many
ended successfully, and how many terminated with failures.

### Syzkaller

Syzkaller is said to be a state-of-the-art Linux kernel fuzzer
{% include cite.html id="lf:syzkaller" %}. The syzbot system is a robot
developed as part of the syzkaller project that continuously fuzzes main Linux
kernel branches and automatically reports found bugs to kernel mailing lists.
Syzbot can test patches against bug reproducers. This can be useful for testing
bug fix patches, debugging, or checking if the bug still happens. While syzbot
can test patches that fix bugs, it does not support applying custom patches
during fuzzing. It always tests vanilla unmodified git trees. Nonetheless, one
can always run syzkaller locally on any kernel for better testing a particular
subsystem or patch {% include cite.html id="syzkaller:syzbot" %}. Syzbot is
receiving increasing attention from kernel developers. For instance, Sasha Levin
said that he hoped that failure reproducers from syzbot fuzz testing could be
added as part of testing for the stable tree at some point
{% include cite.html id="lwn:stable-stab" %}.

Syzkaller is accessible from a GitHub
[repository](https://github.com/google/syzkaller). The project received various
contributions in the length of time close to our evaluation window. The majority
of those changes were committed by five core developers. Also, the Syzkaller
project [mailing list](https://groups.google.com/forum/\#!forum/syzkaller) had
several messages recent to our evaluation period.

The Syzkaller documentation is fairly complete. It contains detailed
instructions on how to install and use Syzkaller as well as several
troubleshooting sections with tips against possible setup problems. The
documentation also includes pages describing how the fuzzer works, how to report
bugs found in the Linux kernel, and how to contribute to the tool.

When run, Syzkaller prints execution environment information to the terminal and
activates an HTTP server. The server pages display detailed test information
such as code coverage, the number of syscall sequences executed, number of
crashes, execution logs, etc.

### LTP

The Linux Test Project (LTP) is a test suite that contains a collection of
automated and semi-automated tests to validate the reliability, robustness, and
stability of Linux and related features {% include cite.html id="lj:lk-testing" %}
{% include cite.html id="ltp:howto" %}. By default, LTP run script include tests
for filesystems, disk I/O, memory management, inter process comunication (IPC),
the process scheduler, and the system call interface. Moreover, the test suite
can be customized by adding new tests, and the LTP project welcomes
contributions {% include cite.html id="lj:lk-testing" %}.

Some Linux testing projects are built on top of LTP or incorporate it somewhat.
For example, LTP was chosen as a starting point for Lachesis, whereas the LAVA
framework provides commands to run LTP tests from within
it {% include cite.html id="lj:lk-testing" %}. Another test suite that runs LTP
is LKFT {% include cite.html id="lf2020:hist" %}
{% include cite.html id="linaro:lkft-tests" %}.

LTP is available at a GitHub
[repository](https://github.com/linux-test-project/ltp) at which many developers
committed in the weeks preceding the evaluation period. There were also a few
discussions in progress on the project's [mailing list](https://lore.kernel.org/ltp/).
In addition, the LTP documentation contains a tool installation and usage guide
as well as other information we found helpful.

We ran a few LTP syscall tests separately and had a pleasing first impression of
the test suite. The completion time of each test was short, and their results
(pass or fail) were very clear. It took about 30 minutes to run the entire
collection of system call tests. LTP also has a set of device driver tests, but
many of them are outdated and do not work anymore.

### ktest

ktest provides an automated test suite that can build, install, and boot test
Linux on a target machine. It can also run post boot scripts on the target
system to perform further testing{% include cite.html id="lj:lk-testing" %}
{% include cite.html id="oracle:ktest" %}. ktest has been included in the Linux
kernel repository under the directory *tools/testing/ktest*. The tool consists
of a perl script (ktest.pl) and a set of configuration files containing test
setup properties. In addition to the build and boot tests, ktest also supports
git bisect, config bisect, randconfig, and patch check as additional types of
tests. If a cross-compiler is installed, ktest can also run cross-compile tests
{% include cite.html id="elinux:ktest" %}{% include cite.html id="lj:lk-testing" %}.

ktest is available from within the Linux kernel repository under the
*tools/testing/ktest/* directory. Despite belonging to the kernel project, the
last contribution to ktest dates from five months before our inspection date.
Its documentation is sparse and has only a description of the configuration
options along with a brief description of the existing example configuration
files. There is no installation guide, nor any list of test dependencies. To set
up ktest, we followed the guidelines provided by Jordan
{% include cite.html id="oracle:ktest" %} and adapted several runtime
configurations. We only ran an elementary build and boot test over a couple of
patches. Nevertheless, we think ktest could be useful in automating many test
activities mentioned in the literature, such as patch checking, bisecting, and
config bisect.

### Smatch

Smatch (the source matcher) is a static analyzer developed to detect programming
logic errors. For instance, smatch can detect errors such as attempts to unlock
already unlocked spinlock. It is written in C and uses Sparse as its C parser.
Also, smatch is run on Linux kernel trees by autotest bots such as 0-day, Hulk
Robot {% include cite.html id="lj:lk-testing" %}
{% include cite.html id="smatch:intro" %}{% include cite.html id="lf2020:hist" %}.

We got Smatch by cloning this [repository](https://repo.or.cz/w/smatch.git). The
project's commit history showed us contributions recent to the time we evaluated
the tool, although a single developer had authored the majority of those
changes. The mailing list [archives](https://sourceforge.net/p/smatch/mailman/)
we found have registered no messages for years. Smatch also has a mailing list
at [vger.kernel.org](http://vger.kernel.org/vger-lists.html\#smatch), but we did
not find mail archives for those. The Smatch documentation is brief,
nevertheless, it contains instructions on how to install and use the source
matcher. Within a few minutes, we had set up Smatch and run some static tests
against Linux drivers.

### Coccinelle / coccicheck

Coccinelle is a static analyzer engine that provides a language for specifying
matches and transformations in C code. Coccinelle is used to aid collateral
evolution of source code and to help in catching certain bugs that have been
expressed semantically. Collateral evolution is needed when client code has to
be updated due to development in library API. Renaming a function, adding
function parameters, and reorganizing data structures are examples of changes
that may lead to collateral evolution. Also, bug chase and fixing are made with
the aid of coccicheck, a collection of semantic patches that makes use of the
Coccinelle engine to interpret and run a set of tests. coccicheck is available
in the Linux kernel under a make target with the same name
{% include cite.html id="coccinelle:home" %}
{% include cite.html id="bottest:coccicheck" %}
{% include cite.html id="bottest:start" %}.
Moreover, coccicheck is run on Linux kernel trees by automated test robots such
as 0-day and Hulk robots {% include cite.html id="lf2020:hist" %}
{% include cite.html id="bottest:coccicheck" %}.

Coccinelle can be obtained through the package manager of many GNU/Linux
distributions, as a compressed tar.gz file from the project's web page, or
through a GitHub repository. As of the day we evaluated the static analyzer,
Coccinelle's repository had some recent commits, most of them by a single
developer. Also, the project's [mailing list](https://lore.kernel.org/cocci/)
had ongoing conversations and patches under review.

The Linux kernel documentation has a page with installation and usage
instructions for both Coccinelle and coccicheck. Moreover, the kernel has a
Makefile target named "coccicheck" for running coccicheck semantic patches. In a
few minutes, we installed Coccinelle and ran some checks on Linux drivers.

### jstest

jstest is a userspace utility program that displays joystick information such as
device status and incomming events. It can be used to test the features of the
Linux joystick API as well as for testing the functionality of a joystick
driver {% include cite.html id="kdoc:xpad" %}
{% include cite.html id="kdoc:joystick" %}
{% include cite.html id="jstest:manual" %}.

jstest is part of the Linux Console Project and can be obtained from
[Source Forge](https://sourceforge.net/projects/linuxconsole/) or through the
package manager of some GNU/Linux distributions. However, as of the date we
evaluated jstest, the project's repository was about a year without updates and
the associated [mailing lists](https://sourceforge.net/p/linuxconsole/mailman/)
were without discussions or patches for even longer. Despite that, the jstest
documentation was helpful as it listed dependency packages and the installation
steps for the tool. Also, the manual page is brief yet informative, containing
what you need to use the tool. To use jstest, one must provide the path to a
joystick or gamepad device. jstest displays the inputs obtained from joysticks
and gamepads and thus can be used to test the functioning of drivers for these
devices in a black-box fashion.

### TuxMake

TuxMake, by Linaro, is a command line tool and Python library designed to make
building the Linux kernel easier. It seeks to simplify Linux kernel building by
providing a consistent command line interface to build the kernel across a
variety of architectures, toolchains, kernel configurations, and make targets.
By removing the friction of dealing with different build requirements, TuxMake
assists developers, especially newcomers, to build test the kernel for uncommon
toolchain/architecture combinations. Moreover, TuxMake comes with a set of
curated portable build environments distributed as container images. These
versioned and hermetic filesystem images make it easier to describe and
reproduce builds and build problems. Although it does not support every Linux
make target, the TuxMake team plans to add support for additional targets such
as kselftest, cpupower, perf, and documentation. TuxMake is part of TuxSuite,
which in turn makes part of Linaro's main Linux testing effort
{% include cite.html id="lwn:tuxmake" %}
{% include cite.html id="tuxmake:readme" %}
{% include cite.html id="linaro:systest" %}.

TuxMake is available from its GitLab
[repository](https://gitlab.com/Linaro/tuxmake) and can also be downloaded as a
package for many GNU/Linux distros. The contributions to the project's
repository are recent to the date we evaluated the tool. In addition, the
TuxMake documentation contains installation instructions and examples of how to
use the tool. We note, however, that TuxMake focuses on build testing and thus
only builds the artifacts bound to make targets, not triggering the execution of
further tests cases even when those targets would do so by default.

### Sparse

Sparse (semantic parser) is a semantic checker for C programs originally written
by Linus Torvalds to support his work on the Linux kernel. Sparse does semantic
parsing of source code files in a few phases summarized as full-file
tokenization, pre-processing, semantic parsing, lazy type evaluation, inline
function expansion, and syntax tree simplification
{% include cite.html id="lwn:sparse" %}. The semantic parser can help test C
programs by performing type-checking, lock checking, value range checking, as
well as reporting various errors and warnings while examining the code
{% include cite.html id="kdoc:sparse" %}{% include cite.html id="lwn:sparse" %}.
In fact, Sparse also comprises a compiler frontend capable of parsing most ANSI
C programs as well as a collection of compiler backends, one of which, is a
static analyzer that takes the same name {% include cite.html id="sdoc:welcome" %}.

The kernel build system has a couple of make options that support checking code
with static analyzers, and it uses Sparse as the default checker
{% include cite.html id="lj:lk-testing" %}.
Also, autotest robots such as 0-day and Hulk Robot run Sparse on kernel trees
they test {% include cite.html id="lf2020:hist" %}.

<!-- TODO uso básico -->

<!--
It's up to the developer whether to run these tests on the whole source tree or
to just run them over a specific file or directory.
-->


<!--
TODO falar do kuint

TODO e o Hulk Robot? "mensão honrosa"?
-->

<!--//### academia -->

### SymDrive

Renzelmann et al. {% include cite.html id="Renzelmann2012" %} focused on testing
Linux kernel device drivers using symbolic execution. This technique consists of
replacing a program's input with symbolic values. Rather than using the actual
data for a given function, symbolic execution comes up with input values
throughout the range of possible values to each parameter. SymDrive intercepts
all calls into and out of a driver with stubs that call a test framework and
checkers. Stubs may invoke checkers passing the set of parameters for the
function under analysis, the function's return, and a flag indicating whether
the checker is running before or after the function under test. Driver state can
be accessed by calling a supporting library. Thus, checkers can evaluate the
behavior of a function under test from the execution conditions and the obtained
results.

Symdrive stood out between related works as a testing tool for drivers in the
kernel through symbolic code execution. To set up Symdrive, we followed the
installation steps listed on their developer's page
{% include cite.html id="symdrive:setup" %}. One of the first steps of the setup
consists of compiling and installing S2E, a software platform that provides
functionalities for symbolic execution on virtual machines. The S2E
documentation mentions the use of Ubuntu as a prerequisite for setting up the
{% include cite.html id="s2e:envdoc" %}{% include cite.html id="s2e:envgit" %}
{% include cite.html id="s2e:building" %}, yet, we found indications of
compatibility with other OSes after inspecting the compilation and installation
scripts. Also, we encountered installation error messages notifying us that S2E
is compatible only with a restricted set of processors. However, even though we
had configured the VM with a compatible processor, our 10GB of system memory was
not enough to prevent installation scripts from failing due to lack of RAM.

We found out that the S2E mailing list was semi-open, meaning that only
subscribed addresses may send emails to it. To subscribe to the S2E list,  a
moderator must first approve your subscriptions request. However, it took a
month for an S2E moderator to accept our subscription request to their mailing
list. By the time they granted access to us, we were assessing other testing
tools and did not want to come back to this one. Finally, our email to the
authors of the SymDriver paper was unanswered. So, after a series of setbacks
related to installation and lack of access to support, we gave up on installing
S2E and evaluating the use of the SymDrive tool.

### FAU machine

Buchacker and Sieh {% include cite.html id="Buchacker2001" %} developed a
framework for testing fault tolerance of GNU/Linux systems by injecting faults
in an entirely simulated running system. FAU machine runs a User Mode Linux
(UML) port of the Linux kernel, which maps every UML process onto a single
process in the host system. Thus, a complete virtualized machine runs on top of
a real-world Linux machine as a single process. For injecting faults into the
virtualized system, the framework launches a second process in the host system.
Every time a UML process makes a system call, return from a system call, or
receives a signal, it is stopped by the auxiliary host process. The host process
then decides whether the halted process will continue with or without the signal
received, if errors should be returned from system calls instead of the actual
value, and so on. This technique of virtualization combined with the
interception of processes has the benefits of maintaining binary compatibility
of programs, allowing fault injection in core kernel functionalities, peripheral
faults, external faults, real-time clock faults, and interrupt/exception faults.

To test with FAU machine, we firts asked the authors of
{% include cite.html id="Buchacker2001" %} to point out the tool's repository.
Next, we downloaded the associated repositories and installed the packages
needed for build and installation. The project documentation is outdated and is
not maintained by the developers. For instance, two packages indicated in the
documentation as necessary for the build are deprecated and no longer needed. In
reply to one of our messages, the project maintainer said that questions could
be answered by email: "*Just forget \*any\* documentation you find regarding
FAUmachine. None is correct any more. Sorry for that. We just don't have time to
update these documents. I think you must ask your questions using e-mail.*".

After compiling and installing the FAU machine, we tried to run some tests by
setting up an example from FAU source files. The experiment consisted of
starting a virtual machine and installing a Debian image on its disk. However,
the experiment run script failed during image installation. Our following email
to the maintainer asking for help with the experiment went unanswered. Still,
within the menus and options in the virtual machine management window, it was
possible to see items referencing system fault injection. The evaluation of
these tests, however, cannot be completed.

### ADFI

Cong et al. {% include cite.html id="Cong2015" %} introduced a tool that
generates fault scenarios for testing device drivers based on previously
collected runtime traces. ADFI hooks internal kernel API so that function calls
and return values are intercepted and recorded in trace files. A fault scenario
generator takes trace files as input and iteratively produces fault scenarios
where an intercepted return to a driver is replaced by a fault. Each fault
scenario is then run, and the resulting stack traces are collected to feed
further iterations of the fault scenario generator. ADFI employs this test
method aiming to assess driver error handling code paths that, otherwise, would
rarely be followed.

According to  {% include cite.html id="Cong2015" %}, the efforts to run ADFI
include (1) preparing a configuration file for driver testing; (2) crash
analysis; and (3) (optionally) compilation flag modification to support test
coverage. ADFI automatically runs each generated fault scenario, one after
another, so test execution is automated.

Nevertheless, there is no link or web page address for the ADFI project
repository in the article we found about the tool. Moreover, the authors did not
respond to our email asking how to get ADFI. Thus, it was not possible to
evaluate ADFI as we could not even get the tool.

### EH-Test

Bai et al. {% include cite.html id="Bai2016" %} focused on device driver testing
through a similar approach. They developed a kernel module to monitor and record
driver runtime information. Further, a pattern-based fault extractor takes
runtime data plus driver source code and kernel interface functions as input and
extracts target functions from them. EH-Test considers target functions taking
into account driver-specific knowledge such as function return types and whether
values returned by functions are checked inside some `if` statement. The C
programming language has no built-in error handling mechanism (such as
`try-catch`), so developers often use an `if` statement to decide whether error
handling code should be triggered in device drivers. Then, a fault injector
module generates test cases in which target function returns are replaced by
faulty values. Finally, a probe inserter generates a separate loadable driver
for each test case. These loadable driver modules have target function calls
replaced by an error function in their code.

As for evaluating EH-Test, we downloaded the tool's source code and, with some
adjustments, we managed to build some of the test modules. However, some EH-Test
components do not build with current GCC and LLVM versions. We mailed the main
author asking for some installation and usage guidance, but we had no feedback.

### COD

B. Chen et al. {% include cite.html id="Chen2020" %} presented a test approach
based on hybrid symbolic-concrete (concolic) execution. Their work focus on
testing LKM (Linux Kernel Modules) using two main techniques: (1) automated test
case generation from LKM interfaces with concolic execution; (2) automated test
case replay that repeatedly reproduced detected bugs.

During test case generation, the COD Agent component sequentially executes
commands from an initial test case to trigger functionalities of target LKMs
through the base kernel. Two custom kernel modules intercept interactions
between base Linux kernel and LKMs under test and add new tainted values to a
taint analysis engine. When all commands in the test harness are finished, COD
captures the runtime execution trace into a file and sends it to a symbolic
engine. A trace replayer performs symbolic analysis over the captured trace
file, then sends the generated test cases back to the execution environment.
These steps then repeat to produce more test cases until some criteria (such as
elapsed time) are met.

In test case replay mode, COD Test Case Replayer picks a test case and executes
the commands in the test harness to trigger functionalities of target LKMs.
Three custom kernel modules intercept the interactions between kernel and LKMs
under test, modify these interactions when needed, and capture kernel API usage
information. After all commands in the test harness are finished, COD retrieves
the kernel API usage information from the custom kernel modules and checks for
potential bugs. This process repeats for each test case given as input.

Yet, for reasons analogous to ADFI, COD could not be tested either. There is no
repository link in {% include cite.html id="Chen2020" %} or instruction on how
to get COD. We sent an email to the paper authors, but that was unanswered.

### Troll

Rothberg et al. {% include cite.html id="Rothberg2016" %} developed a tool to
generate representative kernel compilation configurations for testing. Troll
parses files locally for configuration options (\#ifdef) and creates a
partial kernel compilation configuration. This initial step is called sampling.
Each partial configuration is then abstracted by a node in a configuration
compatibility graph (CCG). In this graph, mutually compatible configurations are
linked by an edge. In the next step (merging), Troll looks up the CCG for the
largest click (set of nodes that are all linked together) and merges all those
partial configurations that belong to the click. The compilation configuration
obtained with the biggest click covers most of the \#ifdef and generates several
warnings when Sparse analyzes the code generated by such arrangement. With a
valid configuration providing good coverage of different configurations
(\#ifdef), further automated testing is more likely to find bugs.

Since Troll was designed to generate Linux kernel build configurations, it does
not fit into the kernel tests category. Despite that, we decided to give Troll a
try. Nevertheless, on our first shot, we found that some new Kconfig features
were not supported by Undertaker, a software whose output was needed to feed
Troll. Also, the Undertaker mailing list was semi-open. Since our adjustments to
the kernel symbols were insufficient to make Undertaker generate partial kernel
configurations, our last resort was to reach Troll's developers. Surprisingly,
the authors were very responsive and helped us to set up the latest Undertaker
version. After that, we ran an example from Troll documentation that generates
Linux kernel compilation settings. The uses of Troll presented in
{% include cite.html id="Rothberg2016" %} are analogous to the documentation
example we ran but the fact that they require more than 10GB of system memory to
complete. Due to that, we could not reproduce those use cases.

## History

1. V1: Release

{% include print_bib.html %}
