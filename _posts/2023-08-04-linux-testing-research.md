---
layout: post
title:  "Linux kernel device driver testing from maintainers' perspective"
date:   2023-08-13 14:10:11 -0300
ref: linux-driver-testing-research
lang: en
---

<!--begin-references-->

{% include add_ref.html id="foundationsSoftwareTesting"
    author="Aditya P. Mathur"
    title="Foundations of Software Testing"
    publisher="Pearson India"
    year="2013"
    url="https://www.oreilly.com/library/view/foundations-of-software/9788131794760/" %}

<!--end-references-->

The presence of test robots and CI rings testing the Linux kernel may be no new
to most developers. Various test systems, such as 0-day, LKFT, Syzbot, Hulk
robot, CKI, Buildbot, and KernelCI, mess the kernel up daily. These machines
have become the [top bug reporters](https://lwn.net/Articles/895800/) for Linux.
Yet, that doesn't mean individual developers are unimportant for assuring the
kernel works as desired.


As far as your author is aware of, Linux has been mostly written by
flesh-and-bone human beings. And, even though Linux comes with no legal
warranties, kernel developers are likely to test their code either because they
want to avoid bug reports, eager to be recognized for their contributions, are
passionate users of Linux, or because all these. Thus, to bring in a new
perspective on Linux kernel testing, your author and his college supervisors
have surveyed device driver maintainers about Linux test habits.

## A device driver test survey

To help characterize the current device driver test practices and grasp what
could be done to improve the effectiveness of Linux device driver testing, Linux
maintainers were invited to answer a survey on the subject. Since the scope of
this investigative work was limited to device drivers, only those who maintained
artifacts under the *drivers* directory were requested. The invitations were
sent by email from 2022-05-16 to 2022-05-24, and the questionnaire remained
active until 2022-06-26 (roughly a month after the last invitations were sent).
The responses to this survey were anonymized and its data is available
[here](https://gitlab.com/Marcelosc/ime-usp-masters-dissertation/-/blob/dissertation-final/survey/results-survey313985-2022-06-27-anonymized.csv).

### Who answered to the survey?

A total of 85 developers completed the survey, representing 7% of 1211 device
driver maintainers identified within the MAINTAINERS file for the 5.17 Linux
release. Most participants had 6 to 10 years (28%) of kernel development
experience, and many others had 1 to 5 years (26%) of involvement (see Figure 1).
Very few developers say to have less than one year (2%) of experience.
Remarkably, a reasonable share (44%) of the answers came from highly experienced
Linux kernel developers with eleven or more years of experience working on the
project.

{% include add_image.html
   src="dev_exp.png"
   style="width:70%;display:block;margin-left:auto;margin-right:auto;"
   caption="Survey attendants’ years of Linux development experience." %}

As expected, most (82%) respondents consider themselves Linux maintainers. Even
though the high identification rate with the maintainer role might have been
biased by the invitation letter, which addressed recipients as maintainers, it
is interesting to note that some individuals (18%) did not identify themselves
as maintainers. This result suggests that, to some developers, the Linux kernel
maintainership role would be associated with something more than being listed in
the MAINTAINERS file. One possible interpretation is that for those who did not
check the maintainer role option, the maintainer role would be characterized by
additional responsibilities or status, such as keeping development trees or
being part of Linus Torvalds' trust network (see Figure 2).

{% include add_image.html
   src="dev_roles.png"
   style="width:70%;display:block;margin-left:auto;margin-right:auto;"
   caption="Roles survey attendants play within the Linux kernel community." %}

Also, most respondents consider themselves active contributors (66%) or users
(62%) of Linux. Almost half of the respondents said to be reviewers (48%) for
the Linux kernel, and a reasonable share of them declared themselves expert
contributors (31%). Only a small portion of participants considered themselves
software testers (20%). Yet, your author believes that this outcome does not
compromise the survey results concerning test practices. Assuming that most
Linux maintainers have contributed with code, it should be reasonable to agree
with Mathur in that "*it is hard to imagine an individual who assumes the role of
a developer but never that of a tester*."{% include cite.html
id="foundationsSoftwareTesting" %} Likewise, one may think it would be hard to
maintain (or develop) a device driver without ever testing it.

### Test habits

The first question related to Linux test habits sought to assess how often
developers test their code before submitting it to mailing lists and what types
of tools they used for those tests.

For the frequency in which tests are run, 90% of device driver maintainers said
to test their contributions often (always or, at least, most times). This
result indicates that driver maintainers are committed to testing the changes
proposed for the Linux kernel (see Figure 3).

{% include add_image.html
   src="test_habits.png"
   style="width:70%;display:block;margin-left:auto;margin-right:auto;"
   caption="General kernel testing habits of survey participants." %}

Also, device driver maintainers run static analysis tools more often than
dynamic analysis test tools. While 27% of driver maintainers said to use static
analysis tools frequently (always or most of the time), only 11% of maintainers
said to run dynamic analysis tools as often. A possible explanation for such
preference may stem from the different usage requirements for those tools.
Usually, static analysis tools are easier to set up and require less time to run
than dynamic analysis tools.

The types of tests performed by driver maintainers may vary as well. 30% of
driver maintainers who said to always runtime test their patches don't use any
static or dynamic analysis tool. Among maintainers who runtime test patches most
of the time, 17% don't use any static or dynamic analysis tool. These results
indicate that a reasonable number of maintainers test their drivers with other
types of tools, with custom scripts, or manually. Nevertheless, 19% of driver
maintainers who always runtime test their patches said to use static analysis
tools very often (always or, at least, most times), and 15% of them said to use
dynamic analysis tools very often (always or, at least, most times).

To go more specific about the tools used to test Linux, the researchers
conducting this survey included an open-ended question to ask participants if
they ran any particular tool to test the kernel and, if so, which. A total of 39
maintainers answered that question, some of them mentioning several tools in
their responses. After merging names referring to equivalent tools and removing
a few names that didn't seem associated with any tool, the researchers
identified 38 different tools. (see Figure 4) Note, however, that not every tool
mentioned by driver maintainers is known as a test tool or was designed for
software testing. Despite that, it was preferred to include a few non-test tools
in the list than possibly disregarding developers' feedback. The most mentioned
tool for that question was sparse (18%), followed by kselftest (13%), KASAN
(6%), and smatch (6%).

{% include add_image.html
   src="test_tools.png"
   style="width:70%;display:block;margin-left:auto;margin-right:auto;"
   caption="Tools that driver maintainers use to test the Linux kernel." %}


Despite a large number of tools pointed out by survey participants, some
individuals said not to run any test tool or to use custom test scripts: "*No. To
determine whether a driver works only runtime testing is used.*", "*custom scripts
to upload system images to devices I use*", "*shell scripts and simple user api
tests*", "*printk()*". Others observed the activity of automated test tools: "*No,
but I've seen syzbot & others have tested my patches*", "*[several tests]. I rely
on the Linux test bot and community review for the rest*".

In another survey session, Linux maintainers were asked what types of drivers
they have ever tested. The number of driver types reported for the question
approaches a hundred, though many were mentioned only once. Remarkably, about a
third of Linux driver maintainers have been involved in testing platform drivers
(34%), followed closely by PCI and PCI Express drivers (33\%), then network
(29%) (see the table below).

<table style="text-align: center">
<caption> Top ten driver types reported in response to survey question 9.</caption>
<tr><th colspan="2">What types of device drivers have you ever tested?</th></tr>
<tr> <th>Driver Type</th> <th>Count (Percentage)</th></tr>
<tr> <td>platform   </td> <td>29 (34\%)</td></tr>
<tr> <td>PCI/PCIe   </td> <td>28 (33\%)</td></tr>
<tr> <td>network    </td> <td>25 (29\%)</td></tr>
<tr> <td>USB        </td> <td>24 (28\%)</td></tr>
<tr> <td>I2C        </td> <td>16 (19\%)</td></tr>
<tr> <td>input      </td> <td>14 (16\%)</td></tr>
<tr> <td>clock      </td> <td>11 (13\%)</td></tr>
<tr> <td>GPU        </td> <td>11 (13\%)</td></tr>
<tr> <td>SPI        </td> <td>11 (13\%)</td></tr>
<tr> <td>wifi       </td> <td>9 (11\%) </td></tr>
</table>

A following question encouraged participants to report activities they carry
on during device driver testing. However, listing all tasks one might take to
test a device driver is not feasible because that could be a very long list with
several hardware-specific procedures. The Linux Foundation Technical Advisory
Board staff advised your author that the issues involved in writing a driver for
a PCI device are much different than those of writing a USB driver, and even
those are different from writing a driver for a PHY device, and so on.
Therefore, it seemed legitimate to present a question to assess how developers
even test a driver. Nonetheless, three generic activities that could make part
of a testing procedure and an Other option were listed to provide respondents
some guidance. The results for the suggested alternatives are shown in Figure 5.

{% include add_image.html
   src="test_activs.png"
   style="width:70%;display:block;margin-left:auto;margin-right:auto;"
   caption="How Linux maintainers test the drivers they maintain." %}

Most (84%) device driver maintainers said to probe and bind the drivers they
maintain to some device when performing tests. That seems to be good news since
it indicates that most drivers pass at least basic soundness tests.

In addition to the predefined alternatives, each option had a text field where
driver maintainers could share further details about test practices. Analyzing
those comments, the researchers noticed a few interesting remarks denoting that
the execution of tests is sometimes conditioned to the possession of hardware.
Among the comments to the *I probe and bind the driver to a device it supports*
alternative were: "*runtime testing on real hardware*", "*Only if I have that
device*", and "*...or an emulation of the supported device*". Also, one of the
comments to the *Other* option was, "*Reproduce bugs and verify patches fix them.
Always if I have the hardware to reproduce*". These comments indicate that even
though the Linux kernel is free software, it might be hard to test some of its
parts without specific (non-free) hardware devices.

The last objective survey question asked participants about their familiarity
with twenty Linux kernel test tools. Because there is a myriad of Linux kernel
test tools available, the investigation of usage frequency was limited to those
most cited by academic articles and online publications from renowned sources
such as LWN, Linux Journal, linux.com, The Linux Foundation, and The Linux
Kernel documentation (see Figure 6).

{% include add_image.html
   src="test_tools_list.png"
   style="width:70%;display:block;margin-left:auto;margin-right:auto;"
   caption="How familiar are Linux driver maintainers with driver testing tools." %}

Among Linux driver maintainers who completed the survey, the most often used
test apparatus is the 0-day test robot (21% of respondents), followed by Sparse
(19%), then Kselftest (8%). Moreover, Coccinelle is the most known test tool
since it got the lowest number of marks for the *I've never heard about it* and
*No answer* alternatives together. Similarly to what was observed in an earlier
question, Figure 6 also reveals a preference for static analysis tools over
dynamic analysis. An expressive ratio of respondents reported using Sparse
(42%), Smatch (20%), or Coccinelle (16%) with some frequency (sometimes or
often). In contrast, fewer individuals said to use Syzkaller (15%), LTP (7%),
KUnit (7%), Trinity (4%), ktest (2%), jstest (0%), SymDrive (0%), FAU machine
(0%), ADFI (0%), EH-test (0%), or COD (0%) as frequently. The exception to this
tendency was Kselftest (16%), which maintainers said to run roughly as often as
Smatch or Coccinelle.

Another impressive result is the recognition given to the 0-day test robot. As
an automated test service, developers don't use the bot by initiating a test
tool as they would with any locally installed software. Instead, they benefit
from the service through reports sent by 0-day when it finds potential bugs in
code sent to the mailing lists or within monitored development trees. Thus,
despite having no direct control over the start of tests performed by 0-day,
many developers perceive they are using the tool very often.

### Challenges to device driver testing

At the end of the survey, participants were presented with three open-ended
questions to allow them to report any additional information relevant to
discussing device driver testing and expressing their opinions about the
subject. The first of those questions asked if they would like to mention any
other tool for testing device drivers. In addition to the tools previously
reported, Linux maintainers also mentioned KCSAN, and klocwork. Some respondents
also went further by commenting on details of test tools or giving their
opinions about them: "*I don’t know any of the test tools; but in ‘make
menuconfig‘ I noticed that the I2C stack (and probably others, too) support
fault injection.*", "*The intel-gfx / i915 devs have extensive CI for the drm/kms
subsystem. The sparse/build tests are universal (for drm/kms code) the actual
functionality tests are all run on Intel graphics only*", "*gitlab-ci or
equivalent should be used by maintainers to ensure tests are run and pass before
merging.*", "*roadtest for i2c drivers. Not upstream yet but very useful.*" Lastly,
a couple of intriguing comments mentioned QEMU as a supporting tool for device
driver testing: "*QEMU Emulation, Mocked Emulation*", "*It’s not strictly a testing
tool, but qemu can be helpful for testing drivers on emulated hardware*".

The next question asked what maintainers thought were the main challenges for
device driver testing. To that matter, the most mentioned issue was having
**access to the hardware** required to do tests, as reported by 25 respondents. This
outcome should be of moderate concern since it indicates that 29% of device
driver maintainers may find it hard to test changes against real hardware
devices.

> "HW access - I do not have working HW for the stuff I maintain these days."

> "From subsystem maintainer PoV: missing hardware availability."

Another circumstance that may hinder device driver testing is the lack of access
to a specific device variant supported by a driver.

> "Hardware availability for drivers that support many devices with small (or
> not so small) differences in the programming model."

Going for a solution candidate to the hardware availability issue, some survey
participants regarded device emulation as a challenge for advancing driver
testing.

> "Lack of hardware meaning emulation development is often necessary. Sometimes
> stubbing functionality is sufficient"

> "Emulating the device side"

> "Emulating hardware"

Regarded some limitations, the development of virtual devices could be a
solution to the hardware access problem. Virtual devices would enable
drivers to probe and bind to a device, allowing at least a few code paths to be
tested. Further, a custom virtual device would also respond to driver requests,
mocking operations from a hardware counterpart.

However, virtual devices are not a silver bullet. First, an emulated device
would only be able to provide fake data since it would hardly interface with
other devices on the system or ever capture real-world data. Second, designing a
proper device model for some hardware designs might be tricky because they may
have a large state space or non-deterministic behavior. In the words of survey
participants, testing a device driver may be challenging due to the "*lack of
good documentation and behavior models that could be used to test drivers
without the hardware*" or because of "*hardware behavior unpredictability and
range of potential states*". Another maintainer argued that:

> "You need real hardware. Because anything else (eg stubs, emulations, or models)
> have to be bug-for-bug compatible for the testing to be relevant. Real hardware
> also exhibits timing variations/interactions that are nearly impossible to
> replicate any other way, and most of the time you’ll find "undocumented
> features"".

A fourth driver maintainer said that:

> "If you want to control the test rig as much as you do with software testing
> software, you have to first implement an emulation of the hardware to be able to
> e.g. provoke different fault states (fault injection)."

Yet another survey participant reported that:

> "As for writing tests, for example unit tests, it can be really hard to
> accurately model the hardware component. This means that for proper testing, a
> system with the target hardware platform must be available for anyone wanting to
> run such tests. [...] Bots, like the aforementioned syzbot, have no real way to
> test the driver’s functionality (in fact, the only reports I got back from them
> were compile errors for some more obscure platforms)."

While there is no consensus on whether testing device drivers against emulated
hardware is a good or bad idea, it is worth noting that some test strategies
may not suit to some driver classes.

> "At least for media drivers, there are too many possible permutations to test.
> Unit tests typically are not really suitable for these types of drivers, the
> tests have to happen at the system level, hence the development [...] of the
> compliance utilities maintained in https://git.linuxtv.org/v4l-utils.git/".

For network drivers, the dynamics of network packet traffic hinders the
development of representative stress tests.

> "It is very hard to create tests that are not synthetic. For networking, it
> is not trivial to simulate random packet traffic, that to some extent maps to
> real world use case."

In the worst scenarios, hardware designs may differ a lot from each other,
possibly having particular interfaces, register maps, operating modes,
capabilities, execution contexts, and other distinguishing characteristics that
can turn the process of creating test cases into a device-specific task.

The second most mentioned challenge related to device driver testing
is **expanding automated test systems** (6 (7%) driver maintainers).
One of the concerns raised was "*board farm management for testing a
wide diversity of hardware.*" Going further on the automation affair, the
response from another maintainer described a challenge to device driver testing
as:

> "Automating testing - especially automating the hardware feedback loop: By
> capturing video, emulating input devices, designing homebrew storage devices
> with fault injection, etc."

That, of course, can be more problematic than one expects as a different survey
participant pointed out a difficulty in maintaining test loops themselves:

> "Hardware that may fail being part of testing loop and it is hard to distinguish
> in an automated system whether the failure is due to driver bug or a hw
> problem."

Nevertheless, despite the adversities of automated testing, no one objected to
it. In fact, some maintainers have conveyed that automating tests is the path
to trail.

> "Automating the testing can also be sometimes very difficult. Manually testing
> will usually just take too long."

> "Centralizing CI services involves booting boards, which is hard, but is
> necessary for linux to start achieving usable quality at release time."

The last survey question let participants leave suggestions on what could be
done to improve device driver testing. The top three most reported ideas were to
**advance in automated test and CI systems** (mentioned by 9% of maintainers), to
**invest more in hardware emulation, virtual models, or mock frameworks** (8% of
respondents), and to ask for **more support from hardware manufacturers** (5% of
participants).

From those who wanted more automated tests, we note a positive opinion about
current test services and an appeal for hooking more test systems to
development trees.

> "Offline testing, such as KernelCI and 0-day, seem extremely useful to me as
> follow-up for pre-posting (in-house) testing. I’d recommend exploring how such
> approaches can be extended both in terms of coverage and when testing occurs -
> before/after patches are posted/merged."

> "Hire more people to work on testing: writing test scenarios, writing tools for
> automated testing on target HW, writing tools for automated testing via mocks
> without target HW, proving public servers for running automated tests (e.g. for
> every sent patch), increase test coverage, [...]."

> "Improve the discoverability of current processes for hooking into things like
> KernelCI. Another place I think we could improve on is companies that have their
> own testing farms, but that don’t have them publicly accessible."

> "Implement SW-controlled test benches for every bus/protocol using HW that
> supports endpoint and controller roles. Hook that to the specific kernel
> maintainer branches so they are run on every patch inclusion."

An interesting suggestion in the direction of having more tests on hardware
devices considered the possibility of putting efforts in "*making it easy for
testing, and for reporting test results, for those with access to the real
device*".

Most maintainers advocating in favor of emulation were brief. One of them
suggested the community "*invest more in compose-able emulation and mocked
models.*" Another participant proposed to "*write device emulations to test device
drivers in, e.g., QEMU. If available, the tests can even be automated.*" One
driver maintainer, though, reported a positive experience with emulation:

> "For the media subsystem it was very helpful to create virtual drivers (i.e.
> drivers emulating media hardware), as that is very useful to catch regressions
> in core media frameworks, and it allows testing media APIs for hardware types
> that are otherwise very difficult to obtain (if at all)."

Overall, a relatively high number of responses suggested advancing device
mocking, virtualization, or emulation or had commented about those types of
software. A possible explanation for that is that investing in software is seen
as a solution to two major issues reported by maintainers. First, emulation
provides a way of testing device drivers without hardware. Second, software
artifacts may be easier to manage than the hardware devices they emulate,
simplifying the creation and maintenance of automated test environments. Thus,
it is not surprising to see many driver maintainers bringing emulation into the
discussion.

### Okay, maybe we should give more attention to device driver testing, so what?

Considering a scenario where hardware manufacturers continue to release new
designs and devices keep diversifying, the tendency is for the number of device
drivers to grow. Due to that, providing extensive test coverage for Linux kernel
device drivers shall require continuous effort in extending test tooling and
infrastructure as more drivers come in. Despite the lack of consensus, the
feedback from the survey on occasion indicates the community is leaning toward
mocking, virtualization, and emulation implementations to reduce driver test
hurdles. That may be a good opportunity for hardware companies to get closer to
the open source community by fostering software to aid device driver tests. Your
author wonders if, for example, it would be possible to generate a state diagram
from a hardware description (such as a VHDL program) and then use that model to
create a QEMU virtual device to mock the real one. Accessible device mocks would
then boost Linux kernel device driver tests.

Your author would like to thank all Linux kernel maintainers who participated
the device driver testing survey.
Studies like this would be of no interest without your help.

Thank you!

## History

1. V1: Release

{% include print_bib.html %}
