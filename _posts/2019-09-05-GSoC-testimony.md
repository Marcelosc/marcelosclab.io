---
layout: post
title:  "A GSoC Testimony"
date:   2019-09-05 11:20:11 -0300
ref: gsoc-testimony
lang: en
---

Hi, my name is Marcelo, I'm a student at the University of São Paulo, a member
of [FLUSP](https://flusp.ime.usp.br/) and [HLUSP](http://hardwarelivreusp.org/)
students group, and Google Summer of Code (GSoC) student of 2019. Today, I would
like to share my experience throughout the Linux Foundation GSoC project on
Analog Devices AD7292 device driver. The work done during the first 3 months of
the project can be seen here:
[Get the code](https://github.com/marceloschmitt1/linux/tree/GSOC_2019_ad7292).
I also would like to thank my mentors: Dragos Bogdan, Stefan Popa, and Alexandru
Ardelean, who have been providing me guidance from long before the GSoC program
has started.


## A Brief Testimony about the GSoC Experience

The GSoC Analog Devices AD7292 device driver project was a great experience for
me. Throughout the 3 months of the project, I could learn fundamental
characteristics of IIO driver development under the guidance of extremely
skilled mentors who provided me valuable pieces of advice on how to proceed with
developing the AD7292 driver.

The mentors were very supportive and understanding. I have only good things to
speak of them. Overall, the mentors were very responsive, always providing me
the tools and information I needed to proceed with my tasks. They were also
sensitive, kindly giving me some time to focus on my college tasks until I
managed to dedicate the expected amount of time and effort to the project. They
were also very enthusiastic, encouraging me to keep up with a FLOSS student
group at my university. Finally, they were extremely polite, always treating me
with respect and manner. I am very thankful for having them help me with this
project.

To me, it looks like this GSoC project was just the beginning of a great
partnership about learning and developing within the Linux kernel IIO subsystem.

{% include add_image.html
   src="workspace.jpg"
   caption="Me on my workstation with the AD7292 evaluation board." %}

## History

1. V1: Release
