This is the repository for my personal website.

## Installation

Clone this [repository](https://gitlab.com/Marcelosc/marcelosc.gitlab.io).

```
$ git clone git@gitlab.com:Marcelosc/marcelosc.gitlab.io.git
```

## How to run locally

First, configure `install_cache` as the directory for installing the ruby gems.

```
$ bundle config set --local path "install_cache"
```

Next, install the dependencies:

```
$ bundle install
```

Then:

```
$ bundle exec jekyll serve
```

See `.gitlab-ci.uml` for details.

## Based on contrast

This work is based on Niklas Buschmann's 
[contrast](https://github.com/niklasbuschmann/contrast).

## License

© Marcelo Schmitt.
Unless otherwise noted, the content in this website is available under the
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional
</a> license.

Contrast was obtained under the [public domain](http://unlicense.org/).
